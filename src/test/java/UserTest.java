import com.example.daomybatis.User;
import com.example.daomybatis.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class UserTest {
    private  SqlSession getSession() throws IOException {
        SqlSessionFactory fac = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
        SqlSession session = fac.openSession();
        return  session;
    }

    @Test
    public void testSave() throws IOException {
        User u = new User();
        u.setName("Coo");
        u.setHiredate("abc");
        u.setSalary(1);

        SqlSession session = getSession();
        //session.insert("com.example.daomybatis.UserMapper.save",u);

        UserMapper mapper = session.getMapper(UserMapper.class);
        mapper.save(u);

        session.commit();
        session.close();
    }

    @Test
    public void testGet() throws Exception{
        SqlSession session = getSession();
        User u = session.selectOne("com.example.daomybatis.UserMapper.get",1);
        System.out.print(u);
        session.close();
    }

    @Test
    public void testList() throws Exception{
        SqlSession session = getSession();
        List<User> users = session.selectList("com.example.daomybatis.UserMapper.list");
        for (User u:users){
            System.out.print(u);
        }

        session.close();
    }

    @Test
    public void testDelete() throws Exception{
        SqlSession session = getSession();
        User u = session.selectOne("com.example.daomybatis.UserMapper.delete",1);
        System.out.print(u);
        session.close();
    }
}
