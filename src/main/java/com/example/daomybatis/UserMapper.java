package com.example.daomybatis;

import java.util.List;

public interface UserMapper {
    void save(User u);
    void delete(Long id);
    void update(User u);
    User get(Long id);
    List<User> list();
}