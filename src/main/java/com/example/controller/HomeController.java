package com.example.controller;

import com.example.daomybatis.User;
import com.example.daospring.StudentJDBCTemplate;
import com.example.daojdbc.*;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/home")
public class HomeController {
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping("/index")
    public String index() {
        String nowStr = "This is "+DatetimeNow();
        logger.debug("debug-》"+nowStr);
        logger.info(nowStr);
        logger.error("error->"+nowStr);
        return "index";
    }
    private String DatetimeNow(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd Hh:mm:ss");
        String result = sdf.format(date);
        return  result;
    }

    @RequestMapping("/testjdbc")
    @ResponseBody
    public String testjdbc() {
        StudentDao sd = new StudentDao();
        List<Student> students = sd.select();
        String result = "<table>";
        for (int i = 0; i < students.size(); i++) {
            Student s = students.get(i);
            result += "<tr><td>" + s.getId() + "</td><td>" + s.getName() + "</td><td>" + s.getAge() + "</td></tr>";
        }
        result += "</table>";
        return result;
    }

    @RequestMapping("/testspringjdbc")
    @ResponseBody
    public String testspringjdbc() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");
        StudentJDBCTemplate studentJDBCTemplate =
                (StudentJDBCTemplate) context.getBean("studentJDBCTemplate");
        System.out.println("------Records Creation--------");
        studentJDBCTemplate.create("Zara", 11);
        studentJDBCTemplate.create("Nuha", 2);
        studentJDBCTemplate.create("Ayan", 15);
        System.out.println("------Listing Multiple Records--------");

        List<Student> students = studentJDBCTemplate.listStudents();
        String result = "<table>";
        for (int i = 0; i < students.size(); i++) {
            Student s = students.get(i);
            result += "<tr><td>" + s.getId() + "</td><td>" + s.getName() + "</td><td>" + s.getAge() + "</td></tr>";
        }
        result += "</table>";
        return result;
    }

    @RequestMapping("/testmybatisjdbc")
    @ResponseBody
    public String testMyBatisJdbc() throws IOException {
        SqlSessionFactory fac = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-config.xml"));
        SqlSession session = fac.openSession();

        User u = session.selectOne("com.example.daomybatis.UserMapper.get",1);
        session.close();

        System.out.println("------Listing Multiple Records--------");

        String result = "<table>";
        result+="<tr><td>" + u.getId() + "</td><td>" + u.getName() + "</td>";
        result += "</table>";
        return result;
    }
}
