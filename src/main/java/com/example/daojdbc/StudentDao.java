package com.example.daojdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDao {

    Connection conn = null;
    PreparedStatement pst;
    ResultSet rSet;

    DBHelper db = null;



    public List<Student> select() {
        List<Student> list = new ArrayList<Student>();

        String sql = "select * from student";
        try {
            db = new DBHelper();
            try {
                db.initConn();
                conn = db.getConn();
            } catch (Exception e) {
                e.printStackTrace();
            }
            pst = conn.prepareStatement(sql);
            rSet = pst.executeQuery();

            Student student = null;
            while (rSet.next()) {

                student = new Student();
                student.setId(rSet.getInt(1));
                student.setName(rSet.getString(2));
                student.setAge(rSet.getInt(3));
                list.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  list;
    }
}
